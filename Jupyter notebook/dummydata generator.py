#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import pathlib
import numpy as np
import tkinter as tk
from tkinter import filedialog

pad = pathlib.Path('C:\Thuiswerken\SK123\Bulk-testinator\CROHO\CrohoActueel21-9-2020_alleen actueel.xlsx')


root = tk.Tk()
root.withdraw()


file_path = filedialog.askopenfilename(title="Kies CROHO bestand", filetypes=[("Excel files","*.xlsx")])



pad = pathlib.Path(file_path)
croho = pd.read_excel(pad)

kolommen = ['Hoofdopleiding','CROHO','BRIN','Brinvolgnummer','Locatie','Vorm','Afstandsonderwijs','Studiejaar','Email1','Type_Student','Voorkeurstaal','HBO_WO','Leerroute_Track','Kunstopleiding','Email2','Label1','Label2','Label3','Label4','Label5','Label6','Label7','Opleidingsnaamlokaal','Instellingsnaamlokaal','Uitnodigingstekst NL','Uitnodigingstekst EN','Uitnodigingstekst DU','Ondertekening NL_Naam','Ondertekening NL_Functie','Ondertekening EN_Naam','Ondertekening EN_Functie','Ondertekening DU_Naam','Ondertekening DU_Functie']
aanleverformat = croho.reindex(columns=kolommen)
aanleverformat.rename(columns={'Brinvolgnummer':'BrinVolgnummer'}, inplace=True)

def datavullen2(brondf, BRIN=None,aantal_rijen=10, save=False):
    doeldf = pd.DataFrame()
    
    #functie voor overige kolommen vullen met random data
    def randomdata(doeldf, BRIN=None):
        doeldf[['Hoofdopleiding']] = 1
        doeldf[['Afstandsonderwijs']] = np.random.default_rng().integers(2,size=len(doeldf))
        doeldf[['Studiejaar']] = np.random.default_rng().integers(low=1,high=3,size=len(doeldf))
        doeldf[['Type_Student']] = np.random.default_rng().integers(low=1,high=4,size=len(doeldf))
        doeldf[['Voorkeurstaal']] = np.random.default_rng().integers(low=1,high=5,size=len(doeldf))
        doeldf[['HBO_WO']] = np.random.default_rng().integers(low=1,high=3)
        doeldf[['Leerroute_Track']] = 0
        doeldf[['Kunstopleiding']] = np.random.default_rng().integers(2,size=len(doeldf))
        doeldf[['Label1']] = 0
        doeldf[['Label2']] = 0
        doeldf[['Label3']] = 0
        doeldf[['Label4']] = 0
        doeldf[['Uitnodigingstekst NL']] = "Doe mee met de NSE en win mooie prijzen!"
        doeldf[['Uitnodigingstekst EN']] = "Complete our survey and win big money!"
        doeldf[['Uitnodigingstekst DU']] = "Füllen Sie unsere Umfrage aus und gewinnen Sie große Preise!"
        doeldf[['Ondertekening NL_Naam']] = "Dirk Jan van Waggelen"
        doeldf[['Ondertekening NL_Functie']] = "Eindaas"
        doeldf[['Ondertekening EN_Naam']] = "Dirk Jan van Waggelen"
        doeldf[['Ondertekening EN_Functie']] = "Eindaas"
        doeldf[['Ondertekening DU_Naam']] = "Dirk Jan van Waggelen"
        doeldf[['Ondertekening DU_Functie']] = "Eindaas"
        
    #als er een brin opgegeven is, filter op BRIN en kies random rijnummers uit brondf
    if BRIN:
        brindf = brondf.loc[brondf['BRIN']==BRIN].reset_index(drop=True)
        rijnummers = np.random.default_rng().integers(low=0,high=len(brindf)-1,size=aantal_rijen)
        doeldf = doeldf.append(brindf.loc[rijnummers], ignore_index=True)
        doeldf[['Email1']] = ["test" + str(BRIN) + str(x) + "@mwm2.nl" for x in range(1, len(doeldf)+1)]
        doeldf[['Email2']] = ["testemail2" + str(BRIN) + str(x) + "@mwm2.nl" for x in range(1, len(doeldf)+1)]
    else:
        #anders kies random nummers uit brondf
        rijnummers = np.random.default_rng().integers(low=0,high=len(brondf)-1,size=aantal_rijen)
        doeldf = doeldf.append(brondf.loc[rijnummers], ignore_index=True)
        doeldf[['Email1']] = ["test" + str(x) + "@mwm2.nl" for x in range(1, len(doeldf)+1)]
        doeldf[['Email2']] = ["testemail2" + str(x) + "@mwm2.nl" for x in range(1, len(doeldf)+1)]
    
    #voeg random element toe van 1% van de mensen die meerdere opleidingen hebben
    #lijst met indexnummers van 1% maken
    randomdata(doeldf)
    sample = doeldf.sample(frac=0.01).index.tolist()
    #als index de laatste is, neem die van 4 eerder.
    for sam in sample:
        if sam == len(doeldf)-1:
            sam -= 4
        #neem random getal van 1 t/m 4
        aantal = np.random.default_rng().integers(low=1,high=5)
        #kopiëer email1 naar aantal rijen, gebruiker heeft nu meerdere opleidingen
        for i in range(1,aantal):
            doeldf.loc[sam + i, 'Email1'] = doeldf.loc[sam, 'Email1']
            doeldf.loc[sam + i, 'Hoofdopleiding'] = 0
    
    if BRIN and save==True:
        doeldf.to_excel("Output\\" + BRIN + "_" + str(aantal_rijen) + "_.xlsx", sheet_name=BRIN, index=False)
    elif not BRIN and save==True:
        doeldf.to_excel("Output\\bulkbestand_" + str(aantal_rijen) + "_.xlsx",index=False)
        
        
    return(doeldf)