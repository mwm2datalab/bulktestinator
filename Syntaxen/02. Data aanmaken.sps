﻿* Encoding: UTF-8.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* DEEL 1. LOCATIES EN ALGEMENE INSTELLINGEN.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* Geef de locatie van de projectmap.
FILE HANDLE MAP /NAME=
'C:\Thuiswerken\SK123\Bulk-testinator\'.

* Geef de naam van het SPSS databestand (format MWM2 Research Platform).
FILE HANDLE DATA /NAME=
'MAP\CROHO\CrohoActueel21-9-2020_SELECTIE.sav'.

* Instellingen worden uitgelezen en een kopie van het databestand wordt geopend.
OMS /SELECT TABLES 
/IF COMMANDS=['CROSSTABS'] SUBTYPES=['Case Processing Summary'] 
/DESTINATION VIEWER=NO.
SET TLOOK = "Z:\Academy\9. Grafieken en tabellensjabloon\Sjabloon Tabellen\MWM2Look_V2.stt".
SET CTEMPLATE 'Z:\Academy\9. Grafieken en tabellensjabloon\MWM2Grafiek.sgt'.
SET CACHE 5.
SET PRINTBACK = OFF.
GET FILE = DATA.
DATASET NAME Origineel.
DATASET COPY Kopie.
DATASET ACTIVATE Kopie.
DATASET CLOSE Origineel.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* DEEL 2. DUMMYBESTAND MAKEN VOOR SURVEY.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

*SELECT IF BRIN EQ '21PH' OR BRIN EQ '21PI' OR BRIN EQ '22OJ'.

* * * BESTAANDE VARIABELEN HERCODEREN WAAR NODIG.
RECODE VORM ('VOLTIJD'='Vt') ('DEELTIJD'='Dt') ('DUAAL'='Du'). 

* * * OPLEIDINGSNAAM EN INSTELLINGSNAAM VARIABELEN HERNOEMEN.
RENAME VARIABLES (Instellingsnaamlokaal
Opleidingsnaamlokaal=
Instellingsnaam_text
Opleidingsnaam_text).

* * * NUMERIEKE VARIABELEN TOEVOEGEN.
COMPUTE Afstandsonderwijs = RND(RV.UNIFORM(0,1)).
COMPUTE Hoofdopleiding = RND(RV.UNIFORM(0,1)).
COMPUTE Kunstopleiding = RND(RV.UNIFORM(0,1)).
COMPUTE AlgemeneVaardigheden = RND(RV.UNIFORM(0,1)).
COMPUTE WetenschappelijkeVaardigheden = RND(RV.UNIFORM(0,1)).
COMPUTE PraktijkgerichtOnderzoek = RND(RV.UNIFORM(0,1)).
COMPUTE Studieroosters = RND(RV.UNIFORM(0,1)).
COMPUTE Studielast = RND(RV.UNIFORM(0,1)).
COMPUTE Groepsgrootte = RND(RV.UNIFORM(0,1)).
COMPUTE Stages = RND(RV.UNIFORM(0,1)).
COMPUTE Studiefaciliteiten = RND(RV.UNIFORM(0,1)).
COMPUTE Uitdaging = RND(RV.UNIFORM(0,1)).
COMPUTE InternationaleAspecten = RND(RV.UNIFORM(0,1)).
COMPUTE InternationaleStudenten = RND(RV.UNIFORM(0,1)).
COMPUTE Structuur = RND(RV.UNIFORM(0,1)).
COMPUTE Leren = RND(RV.UNIFORM(0,1)).
COMPUTE Reflectie = RND(RV.UNIFORM(0,1)).
COMPUTE Afstand = RND(RV.UNIFORM(0,1)).
COMPUTE Medezeggenschap = RND(RV.UNIFORM(0,1)).
COMPUTE GelijkeBehandeling = RND(RV.UNIFORM(0,1)).
COMPUTE Flexibiliteit = RND(RV.UNIFORM(0,1)).
COMPUTE Kunst = RND(RV.UNIFORM(0,1)).
COMPUTE Studiejaar = RND(RV.UNIFORM(1,3)).
COMPUTE Voorkeurstaal = RND(RV.UNIFORM(1,4)).
COMPUTE HBO_WO = RND(RV.UNIFORM(1,2)).
COMPUTE Inlogcode = RND(RV.UNIFORM(1,1000000)).

FORMATS Afstandsonderwijs TO HBO_WO (F2.0).
FORMATS Inlogcode (F8.0).

* * * STRING VARIABELEN TOEVOEGEN.
STRING Email1
Email2
Uitnodigingstekst_NL
Uitnodigingstekst_EN
Uitnodigingstekst_DU
Ondertekening_NL_Naam
Ondertekening_NL_Functie
Ondertekening_EN_Naam
Ondertekening_EN_Functie
Ondertekening_DU_Naam
Ondertekening_DU_Functie
Opleidingstype
Leerroute_Track
Opleidingsnaam_text_2
Opleidingsnaam_text_3
Opleidingsnaam_text_4
Opleidingsnaam_text_5
LinkPrivacy
Verzenddatum
(A500).

COMPUTE VERZENDDATUM = "NL_D1_S1".

COMPUTE Email1 = CONCAT("TEST", LTRIM(STRING($CASENUM, F8)),"@mwm2.nl").
COMPUTE Email2 = CONCAT("TEST", LTRIM(STRING($CASENUM, F8)),"@crowdtech.nl").
COMPUTE Uitnodigingstekst_NL = "NEDERLANDSE UITNODIGINGSTEKST".
COMPUTE Uitnodigingstekst_EN = "ENGELSE UITNODIGINGSTEKST" .
COMPUTE Uitnodigingstekst_DU = "DUITSE  UITNODIGINGSTEKST" .
COMPUTE Ondertekening_NL_Naam = "NEDERLANDSE ONDERTEKENING" .
COMPUTE Ondertekening_NL_Functie = "NEDERLANDSE FUNCTIE" .
COMPUTE Ondertekening_EN_Naam = "ENGELSE ONDERTEKENING" .
COMPUTE Ondertekening_EN_Functie = "ENGELSE FUNCTIE" .
COMPUTE Ondertekening_DU_Naam = "DUITESE ONDERTEKENING" .
COMPUTE Ondertekening_DU_Functie = "DUITSE FUNCTIE" .

*OPLEIDINGSTYPE.
DO IF CROHO GE 80000 AND CROHO LE 81000.
COMPUTE Opleidingstype = 'Associate Degree'.
ELSE IF (CROHO GE 4000 AND CROHO LE 40000) OR (CROHO GE 50000 AND CROHO LT 60000) OR (CROHO GE 81000 AND CROHO LT 81500).
COMPUTE Opleidingstype = 'bachelor'.
ELSE IF (CROHO GE 40000 AND CROHO LT 50000) OR (CROHO GE 60000 AND CROHO LT 70000).
COMPUTE Opleidingstype = 'master'.
ELSE IF (CROHO GE 70000 AND CROHO LT 75000).
COMPUTE Opleidingstype = 'postiniteel hbo'.
ELSE IF (CROHO GE 75000 AND CROHO LT 80000).
COMPUTE Opleidingstype = 'postiniteel wo'.
END IF.
EXECUTE.



COMPUTE Opleidingstype = "BACHELOR" .
COMPUTE Leerroute_Track = "LEERROUTE NAAM" .
COMPUTE Opleidingsnaam_text_2 = "OPLEIDING_2" .
COMPUTE Opleidingsnaam_text_3 = "OPLEIDING_3" .
COMPUTE Opleidingsnaam_text_4 = "OPLEIDING_4" .
COMPUTE Opleidingsnaam_text_5 = "OPLEIDING_5" .
COMPUTE LinkPrivacy = "HTTPS://LINKNAARPRIVACY.NL" .

ADD FILES FILE=*
/KEEP 
CROHO
BRIN
Locatie
Vorm
Afstandsonderwijs
Studiejaar
Email1
Voorkeurstaal
HBO_WO
Email2
Uitnodigingstekst_NL
Uitnodigingstekst_EN
Uitnodigingstekst_DU
Opleidingsnaam_text
Instellingsnaam_text
Ondertekening_NL_Naam
Ondertekening_NL_Functie
Ondertekening_EN_Naam
Ondertekening_EN_Functie
Ondertekening_DU_Naam
Ondertekening_DU_Functie
Inlogcode
Opleidingstype
Verzenddatum
Leerroute_Track
Hoofdopleiding
Kunstopleiding
Opleidingsnaam_text_2
Opleidingsnaam_text_3
Opleidingsnaam_text_4
Opleidingsnaam_text_5
LinkPrivacy
AlgemeneVaardigheden
WetenschappelijkeVaardigheden
PraktijkgerichtOnderzoek
Studieroosters
Studielast
Groepsgrootte
Stages
Studiefaciliteiten
Uitdaging
InternationaleAspecten
InternationaleStudenten
Structuur
Leren
Reflectie
Afstand
Medezeggenschap
GelijkeBehandeling
Flexibiliteit
Kunst
ALL.

EXECUTE.



SAVE TRANSLATE OUTFILE='C:\Thuiswerken\SK123\Bulk-testinator\Output\PM30372_NSE2021_TESTLINKS SURVEY VOLLEDIG CROHOBESTAND_V3.csv'
  /TYPE=CSV
  /ENCODING='UTF8'
  /MAP
  /DROP BRINVOLGNUMMER
  /REPLACE
  /FIELDNAMES
  /CELLS=VALUES.


SELECT IF BRIN EQ '31PT'.
FREQUENCIES BRIN.

SAVE TRANSLATE OUTFILE='C:\Thuiswerken\SK123\Bulk-testinator\Output\TESTBRIN_31PT.csv'
  /TYPE=CSV
  /ENCODING='UTF8'
  /MAP
  /REPLACE
  /FIELDNAMES
  /CELLS=VALUES.

 




