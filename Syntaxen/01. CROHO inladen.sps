* Encoding: UTF-8.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* DEEL 1. LOCATIES EN ALGEMENE INSTELLINGEN.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* Geef de locatie van de projectmap.
FILE HANDLE MAP /NAME=
'C:\Thuiswerken\SK123\Bulk-testinator\'.

GET DATA
  /TYPE=XLSX
  /FILE='MAP\CROHO\CrohoActueel21-9-2020.xlsx'
  /SHEET=name 'CrohoAct'
  /CELLRANGE=FULL
  /READNAMES=ON
  /DATATYPEMIN PERCENTAGE=95.0
  /HIDDEN IGNORE=YES.
EXECUTE.
DATASET NAME CROHO WINDOW=FRONT.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* DEEL 2. SELECTIE MAKEN OP ACTUEEL EN OP VARIABELEN DIE VAN TOEPASSING ZIJN
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

SELECT IF Codestandrecord EQ 'ACTUEEL'.
FREQUENCIES CODESTANDRECORD.

ADD FILES FILE=*
/KEEP
Opleidingscode
Brinnummer
Gemeentenaam
Opleidingsvorm
Naamonderwijsinstelling
Naamopleidingvoluit
Brinvolgnummer.


EXECUTE.

RENAME VARIABLES 
(Brinnummer
Brinvolgnummer
Naamonderwijsinstelling
Gemeentenaam
Opleidingscode
Naamopleidingvoluit
Opleidingsvorm=
BRIN
BrinVolgnummer
Instellingsnaamlokaal
Locatie
CROHO
Opleidingsnaamlokaal
Vorm).

SAVE OUTFILE='MAP\CROHO\CrohoActueel21-9-2020_SELECTIE.sav'
  /COMPRESSED.


